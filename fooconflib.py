#!/usr/bin/env python

import sqlite

class FooConfContext:

    db = None

    params = {}

    def __init__(self, fname = "fooconf.sqlite"):
        self.db = sqlite.connect(fname, encoding = "utf-8")

        self.params["base-uid"] = 10000
        self.params["base-gid"] = 10000
        self.params["base-home-dir"] = "/home/fooconf"
        self.params["sys-user-name-prefix"] = "fc"
        self.params["sys-group-name-prefix"] = "fc"

    def __del__(self):
        self.db.close()
    

    def getParameter(self, name):
        return self.params[name]

    def makeSysUserName(self, name):
        return self.getParameter("sys-user-name-prefix") + name

    def makeSysGroupName(self, name):
        return self.getParameter("sys-group-name-prefix") + name

    def makeSysUID(self, id):
        return self.getParameter("base-uid") + id

    def makeSysGID(self, id):
        return self.getParameter("base-gid") + id

    def makeHomeDir(self, name):
        return self.getParameter("base-home-dir") + "/" + name

    
