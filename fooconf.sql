

CREATE TABLE account (id INTEGER PRIMARY KEY, name VARCHAR, email VARCHAR, enabled BOOLEAN, password VARCHAR);

-- ACTION: e.g. cron, php, cgi, wedav...
CREATE TABLE permission (id INTEGER, action VARCHAR);

CREATE TABLE mailbox (name VARCHAR, password VARCHAR, owner INTEGER, enabled BOOLEAN);

-- TYPE: e.g. forward, mailbox
CREATE TABLE mail_alias (local VARCHAR, domain VARCHAR, type CHAR, recipient VARCHAR);

CREATE TABLE domain (name VARCHAR PRIMARY KEY, owner INTEGER);

CREATE TABLE domain_vhost (name VARCHAR PRIMARY KEY, enabled BOOLEAN, origin_email VARCHAR);
CREATE TABLE domain_alias (name VARCHAR PRIMARY KEY, vhost VARCHAR);

-- TYPE: proxy, redirect
CREATE TABLE domain_redirect (name VARCHAR PRIMARY KEY, destination VARCHAR, type VARCHAR);

