all:
	mkdir -p output
	./fooconf-unix-passwd > output/passwd
	./fooconf-unix-group > output/group
	./fooconf-postfix-mailbox-domains > output/postfix-mailbox-domains
	./fooconf-postfix-mailbox-map > output/postfix-mailbox-map
	./fooconf-postfix-alias-map > output/postfix-alias-map
	./fooconf-apache > output/apache.conf
	./fooconf-cron > output/cron.allow

reset:
	rm -f fooconf.sqlite
	sqlite fooconf.sqlite < fooconf.sql

clean:
	rm -rf output
	rm -f *.pyc

.PHONY: reset all
